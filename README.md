# Xenharmlib

Xenharmlib is a music theory library for the exploration and research of
microtonality, diatonic set theory, non-standard notations, and many
more. The library implements a superset of Western classical music theory,
so you can also use it to compose and analyze music in the boundaries of
the common practice period or 20th century Western music.

[Click here for the official documentation](https://xenharmlib.readthedocs.io/en/latest/)

So far it supports the following:

* Equal division tunings (e.g. Western, Modern Arabic, Bohlen-Pierce)
* Analysis of intervals, scales, and their relations to one another
* Group theoretical analysis (integer pitches, pitch classes, etc)
* Up/Down Notation (a superset of Western accidental notation)
* Building blocks for custom notations

Coming soon:

* Maximally even sets
* Interval vectors and related properties
* Key signature support for Up/Down Notation scales
* Scale and triad generators
* Just Intonation and Prime Limit Tunings
* Odd Limit Tunings
* Extended Helmholtz-Ellis JI Pitch Notation
