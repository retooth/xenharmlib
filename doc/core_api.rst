=======================
Core API documentation
=======================

Frequency
------------------------

.. automodule:: xenharmlib.core.frequencies
    :members:

Origin Context
------------------------

.. automodule:: xenharmlib.core.origin_context
    :members:

Frequency Representations
---------------------------

.. automodule:: xenharmlib.core.freq_repr
    :members:

Scales
---------------------------

.. automodule:: xenharmlib.core.scale
    :members:

Intervals
---------------------------

.. automodule:: xenharmlib.core.interval
    :members:

Tuning
------------------------

.. automodule:: xenharmlib.core.tunings
    :members:

Pitch and PitchInterval
---------------------------

.. automodule:: xenharmlib.core.pitch
    :members:

PitchScale
---------------------------

.. automodule:: xenharmlib.core.pitch_scale
    :members:

Notation
---------------------------

.. automodule:: xenharmlib.core.notation
    :members:

Note and NoteInterval
---------------------------

.. automodule:: xenharmlib.core.notes
    :members:

NoteScale
---------------------------

.. automodule:: xenharmlib.core.note_scale
    :members:

Enharmonic Strategies
------------------------

.. automodule:: xenharmlib.core.enharm_strategies
    :members:


Symbol Codes
---------------------------

.. automodule:: xenharmlib.core.symbols
    :members:

Exceptions
------------------------

.. automodule:: xenharmlib.exc
    :members:
